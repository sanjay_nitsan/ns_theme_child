<?php
// TYPO3 Security Check
if (!defined('TYPO3')) {
    die('Access denied.');
}
// Extension key
$_EXTKEY = 'ns_theme_child';
// Add default include static TypoScript (for root page)
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', '[NITSAN] Child theme');
