<?php

// Provide detailed information and depenencies of EXT:ns_theme_child
$EM_CONF['ns_theme_child'] = [
    'title' => '[NITSAN] TYPO3 Child Theme',
    'description' => 'The child theme of EXT:ns_basetheme',
    'category' => 'templates',
    'author' => 'Team NITSAN',
    'author_email' => 'info@nitsan.in',
    'author_company' => 'NITSAN Technologies Pvt Ltd',
    'state' => 'stable',
    'version' => '12.0.0',
    'constraints' => [
        'depends' => [
            'ns_basetheme' => '12.0.0-12.5.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
    //'autoload' => array(
    //	'classmap' => array('Classes/'),
    //),
];
